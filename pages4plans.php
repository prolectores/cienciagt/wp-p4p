<?php
/*
Plugin Name: Pages4Plans
Plugin URI: http://cienciagt.org
Description: Funcionalidades varias para administrar páginas de seguimiento de planes de proyectos.
Version: 0.15.20200709
Author: I. Mendoza <dev@ivanmendoza.net>
Author URI: http://gitlab.com/balamaqab
*/

const P4P_plan_Page_Slug = "planes";
const P4P_default_block_title = "Plantilla: Plan de CienciaGT";

require_once("setup.php");
require_once("functions.php");
require_once("ui.php");

register_activation_hook(__FILE__, 'p4p_activation');

if (function_exists("get_fields")) {
    add_action('wp_enqueue_scripts', 'p4p_enqueue_scripts');
    add_filter('the_content',  'p4p_add_bars');
    add_shortcode('p4p_plan_grid', 'p4p_plan_grid_shorcode');
}

add_filter('default_content', 'p4p_default_content', 10, 2);

function p4p_add_bars($content)
{
    global $post;
    if ($post->post_type != 'plan') {
        return $content;
    }
    return p4p_get_informative_bar($post->ID) . $content . p4p_get_navigation_bar($post->ID, $post->post_parent);
}

function p4p_enqueue_scripts()
{
    wp_register_style('p4p_styles', plugins_url('/assets/p4p.min.css', __FILE__), false, "0.15.20200709");
    wp_enqueue_style('p4p_styles');
}
