.p4p_plan_grid {
  width: 100%;

  .status_bar {
    width: 100%;
    border-bottom: solid 1px #aaa;
    margin-bottom: 1em;
    display: flex;
    flex-direction: row wrap;
    justify-content: space-between;

    .item {
      font-size: 1em;
      color: #888;
      line-height: 1.125;

      &.total {
        color: #333;
        font-size: 1.25em;
        font-weight: 600;
      }

      &.summary {
        font-size: 0.875em;
        padding-top: 0.5em;
      }

      &.left {
        text-align: left;
      }

      &.center {
        text-align: center;
      }

      &.right {
        text-align: right;
      }
    }
  }

  .grid {
    width: 100%;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
  }

  .plan {
    margin: 0 0 1em;
    flex: 1 0 100%;
    overflow: hidden;
    box-shadow: 0 0.125em 0.25em rgba(0, 0, 0, 0.12);
    border-radius: 0.25em;

    .areas {
      width: 100%;
      margin: 0;
      padding: 0.75em 1em;
      font-size: 0.625em;
      line-height: 1.25;
      text-transform: uppercase;
      background: #3587c5;
      color: rgba(255, 255, 255, 0.8);
      border-radius: 0.25em 0.25em 0 0;

      a.area {
        white-space: nowrap;
        display: inline-block;
        border-radius: 0.25em;
        text-decoration: none;
        color: rgba(255, 255, 255, 0.8);
        box-shadow: none;

        &:hover {
          color: rgba(255, 255, 255, 1);
        }
      }

      &:after {
        content: ":";
      }
    }

    .title_area {
      display: flex;
      flex-direction: column;
      width: 100%;
      height: 5em;
      padding: 0 0.5em 0.5em;
      justify-content: center;
      border: solid 1px #ddd;
      border-width: 0 1px 0;

      a.title {
        display: inline;
        font-size: 1.125em;
        text-decoration: none;
        border: none;
        font-weight: 600;
        color: #004377;
        line-height: 1.125;
        box-shadow: none;
        vertical-align: middle;
      }
    }

    .metadata {
      display: flex;
      flex-flow: row;
      justify-content: space-between;
      padding: 0 0.25em 0.25em;
      cursor: none;
      user-select: none;
      border: solid 1px #ddd;
      border-width: 0 1px 1px;
      border-radius: 0 0 0.25em 0.25em;

      background-image: linear-gradient(180deg,
          #ffffff 0%,
          #f3f3f3 75%,
          #e6e6e6 100%);

      .item {
        font-size: 0.875em;

        &.status {
          flex: 1 64%;
          text-align: left;
        }

        &.people {
          flex: 1 16%;
          text-align: right;
        }

        &.progress {
          flex: 1 16%;
          font-weight: 600;
          color: #555;
          line-height: 1.75;
          text-align: right;
        }
      }
    }
  }
}

@media screen and (min-width: 768px) {
  .p4p_plan_grid {
    .plan {
      flex: 1 0 30%;
      margin: 0 1em 1em;
    }
  }
}