<?php

function p4p_get_plans($custom_query = array())
{
    $default_query = array(
        'post_type'         => 'plan',
        'post_status'       => 'publish',
        'numberposts'       => -1,
        'meta_key'          => 'status',
        'orderby'           => 'meta_value',
        'order'             => 'ASC',
    );
    $query = array_merge($default_query, $custom_query);
    $plans = get_posts($query);
    return $plans;
}

function p4p_get_areas($postid)
{
    return wp_get_post_terms($postid, array('plan-area'));
}


function p4p_get_navigation_pages($postid, $postparent)
{
    // https://wordpress.stackexchange.com/questions/54422/prev-next-child-navigation-for-current-page

    $pagelist = get_pages("child_of=" . $postparent . "&parent=" . $postparent . "&sort_column=menu_order&sort_order=asc");
    $pages = array();
    foreach ($pagelist as $page) {
        $pages[] += $page->ID;
    }

    $current = array_search($postid, $pages);
    $siblings = array();
    if ($current - 1 >= 0) {
        $siblings["previous"] = get_post($pages[$current - 1]);
    }
    if ($current + 1 < count($pages)) {
        $siblings["next"] = get_post($pages[$current + 1]);
    }
    return $siblings;
}

function p4p_plan_grid_shorcode($atts, $content = null)
{
    $grid = p4p_get_plan_grid();
    return $grid;
}

function p4p_get_template($template_name = "default_content")
{
    $filename = plugin_dir_path(__FILE__) . "/templates/" . $template_name . ".html";
    if (file_exists($filename)) {
        return file_get_contents($filename);
    }
    return "";
}

function p4p_reusable_block_exists($title)
{
    $results = get_posts(array(
        "title" => $title,
        "post_type" => "wp_block",
        "post_per_page" => 1
    ));

    if (count($results) > 0) {
        return $results[0];
    }

    return false;
}

function p4p_create_reusable_block($title, $content)
{
    return wp_insert_post(array(
        'post_content'   => $content,
        'post_title'     => $title,
        'post_type'      => 'wp_block',
        'post_status'    => 'publish',
        'comment_status' => 'closed',
        'ping_status'    => 'closed',
        'guid'           => sprintf('%s/wp_block/%s', site_url(), sanitize_title($title))
    ));
}

function p4p_create_reusable_block_from_template($title, $template)
{
    return p4p_create_reusable_block($title, p4p_get_template($template));
}

function p4p_get_default_content()
{
    $content = "";

    $block = get_posts(array(
        "title" => P4P_default_block_title,
        "post_type" => "wp_block",
        "post_per_page" => 1
    ));

    if (count($block) > 0) {
        $content = $block[0]->post_content;
    } else {
        $content = p4p_get_template();
        p4p_create_default_content();
    }

    return $content;
}

function p4p_default_content($content, $post)
{
    if ($post->post_type == "plan") {
        $content = p4p_get_default_content();
    }
    return $content;
}

function p4p_create_default_content()
{
    $default_block = p4p_reusable_block_exists(P4P_default_block_title);
    if ($default_block == false) {
        p4p_create_reusable_block_from_template(P4P_default_block_title, "default_content");
    }
}
