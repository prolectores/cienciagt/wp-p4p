<?php

function p4p_activation()
{
    p4p_rewrite_flush();
    p4p_create_default_content();
}

add_action('init', 'p4p_plan_post_type', 0);
function p4p_plan_post_type()
{
    register_post_type('plan', array(
        'label'               => __('Planes', 'p4p'),
        'description'         => __('Planes científicos', 'p4p'),
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'taxonomies'          => array('plan-area'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-admin-page',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest'        => true,
        'rewrite'             => array('slug' => 'planes/%plan-area%'),
        'labels'              => array(
            'name'                => _x('Planes', 'Post Type General Name', 'p4p'),
            'singular_name'       => _x('Plan', 'Post Type Singular Name', 'p4p'),
            'menu_name'           => __('Planes', 'p4p'),
            'parent_item_colon'   => __('Plan padre', 'p4p'),
            'all_items'           => __('Todos los Planes', 'p4p'),
            'view_item'           => __('Ver Plan', 'p4p'),
            'add_new_item'        => __('Agregar nuevo Plan', 'p4p'),
            'add_new'             => __('Agregar nuevo', 'p4p'),
            'edit_item'           => __('Editar Plan', 'p4p'),
            'update_item'         => __('Actualizar Plan', 'p4p'),
            'search_items'        => __('Buscar Plan', 'p4p'),
            'not_found'           => __('Plan no encontrado', 'p4p'),
            'not_found_in_trash'  => __('Plan no encontrado en la papelera', 'p4p'),
        ),
    ));
}

add_action('init', 'p4p_plan_areas_taxonomies', 0);
function p4p_plan_areas_taxonomies()
{
    register_taxonomy('plan-area', array('plan'), array(
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'labels' => array(
            'name' => _x('Áreas de planes', 'taxonomy general name'),
            'singular_name' => _x('área', 'taxonomy singular name'),
            'search_items' =>  __('Buscar área'),
            'all_items' => __('Todas las áreas'),
            'parent_item' => __('Área padre'),
            'parent_item_colon' => __('Área padre:'),
            'edit_item' => __('Editar área'),
            'update_item' => __('Actualizar área'),
            'add_new_item' => __('Agregar área'),
            'new_item_name' => __('Agregar área'),
            'menu_name' => __('Áreas de planes'),
        ),
    ));
}

function p4p_show_permalinks($post_link, $post)
{
    if (is_object($post) && $post->post_type == 'plan') {
        $terms = wp_get_object_terms($post->ID, 'plan-area');
        if ($terms) {
            return str_replace('%plan-area%', $terms[0]->slug, $post_link);
        }
    }
    return $post_link;
}
add_filter('post_type_link', 'p4p_show_permalinks', 1, 2);

function p4p_rewrite_flush()
{
    p4p_plan_post_type();
    p4p_plan_areas_taxonomies();
    flush_rewrite_rules();
}
