<?php

function p4p_get_plan_grid($custom_query = array())
{
    $plans = p4p_get_plans($custom_query);
    $count_by_status = array();
    $output = '<div class="p4p_plan_grid">';
    ob_start();
?>
    <div class="grid">
        <?php
        foreach ($plans as $plan) {
            $fields = get_fields($plan->ID);
            $title = $plan->post_title;
            $link = get_the_permalink($plan->ID);
            $areas = p4p_get_areas($plan->ID);
            $progress = $fields["progress"];
            $status = $fields["status"]["value"];
            $status_label = $fields["status"]["label"];

            if (!isset($count_by_status[$status])) {
                $count_by_status[$status] = array(
                    "label" => $status_label,
                    "count" => 1
                );
            } else {
                $count_by_status[$status]["count"]++;
            }
        ?>
            <div class="plan">
                <div class=" areas"><?php
                                    $n_areas = count($areas);
                                    foreach ($areas as $ia => $area) {
                                        if ($n_areas > 1 && $ia != ($n_areas - 1)) {
                                            if ($ia == $n_areas - 2) {
                                                $divider = " y ";
                                            } else {
                                                $divider = ", ";
                                            }
                                        } else {
                                            $divider = "";
                                        }
                                        $area_link = get_term_link($area);
                                    ?><a class="item area <?php echo $area->slug; ?>" href="<?php echo $area_link; ?>"><?php echo $area->name; ?></a><?php echo $divider; ?><?php } ?>
                </div>
                <div class="title_area">
                    <a class="title" href="<?php echo $link; ?>"><?php echo $title; ?></a>
                </div>
                <div class="metadata">
                    <div class="item status phase"><?php echo p4p_get_status_widget($fields, 'tiny'); ?></div>
                    <div class="item people"><?php echo p4p_get_people_count_widget($fields, 'tiny'); ?></div>
                    <div class="item progress"><?php echo $progress . "%"; ?></div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
    <?php
    $pages_output = ob_get_contents();
    ob_end_clean();

    $by_status = "";
    $n_status = count($count_by_status);
    $is = 0;
    foreach ($count_by_status as $status) {
        $label = strtolower($status["label"]);
        if ($label == "pausado" && $status["count"] > 1) {
            $label = "pausados";
        }
        if ($n_status > 1 && $is != ($n_status - 1)) {
            if ($is == $n_status - 2) {
                $divider = " y ";
            } else {
                $divider = ", ";
            }
        } else {
            $divider = "";
        }

        $by_status .= $status["count"] . " " . $label . $divider;
        $is++;
    }

    $status_bar = '<div class="status_bar"><div class="item total left">' . count($plans) . ' Planes</div><div class="item summary right">' . $by_status . '.</div></div>';

    $output .= $status_bar;
    $output .= $pages_output;
    $output .= '</div>';
    return $output;
}

function p4p_get_status_widget($plan_fields, $custom_class = '')
{
    $is_status_single = $plan_fields["status"]["value"] == "not_started" ? true : false;

    ob_start(); ?>

    <div class="p4p_status <?php
                            echo $custom_class . " ";
                            echo $plan_fields["status"]["value"] . " ";
                            echo $plan_fields["phase"]["value"] . " "; ?>">
        <?php if (!$is_status_single) {
        ?><span class="phase"><?php echo $plan_fields["phase"]["label"]; ?></span><?php
                                                                                } ?><span class="status <?php
                                                                                                        if ($is_status_single) {
                                                                                                            echo "single";
                                                                                                        } ?>"><?php echo $plan_fields["status"]["label"]; ?></span>
    </div>

<?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}


function p4p_get_people_widget($plan_fields, $custom_class = '')
{

    echo "<pre>";
    var_dump($plan_fields["people"]);
    echo "</pre>";

    ob_start(); ?>

    <div class="p4p_people <?php
                            echo $custom_class . " ";
                            echo $plan_fields["status"]["value"] . " ";
                            echo $plan_fields["phase"]["value"] . " "; ?>">
        <?php if (!$is_status_single) {
        ?><span class="phase"><?php echo $plan_fields["phase"]["label"]; ?></span><?php
                                                                                } ?><span class="status <?php
                                                                                                        if ($is_status_single) {
                                                                                                            echo "single";
                                                                                                        } ?>"><?php echo $plan_fields["status"]["label"]; ?></span>
    </div>

    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}


function p4p_get_people_count_widget($plan_fields, $custom_class = '')
{
    $people_count = count($plan_fields["people"]);
    $label = $people_count > 1 ? "personas" : "persona";
    $content = "";
    if ($people_count > 0) {
        ob_start(); ?>
        <div class="p4p_people_count <?php echo $custom_class; ?>">
            <span class="icon"></span> <span class="count"><?php echo $people_count; ?></span> <span class="label"><?php echo $label; ?></span>
        </div>
    <?php
        $content = ob_get_contents();
        ob_end_clean();
    }
    return $content;
}


function p4p_get_progress_widget($plan_fields, $min = 0)
{
    $progress = intval($plan_fields["progress"]);
    $bar_class = ($progress  > 70) ? "excelent" : ($progress >= 50 ? "ok" : "bad");
    $content = "";
    if ($progress > $min) {
        ob_start();
    ?>
        <div class="p4p_progress">
            <div class="bar <?php echo $bar_class; ?>" style="width:<?php echo $plan_fields["progress"] . "%"; ?>;"></div>
            <span class="label" style="width:<?php echo $plan_fields["progress"] . "%"; ?>;"><?php echo $plan_fields["progress"]; ?>%</span>
        </div>
    <?php
        $content = ob_get_contents();
        ob_end_clean();
    }
    return $content;
}

function p4p_get_informative_bar($postid)
{
    $fields = get_fields($postid);
    ob_start();
    ?>
    <div class="php4_informative_toolbar">
        <div class="status_area"><?php echo p4p_get_status_widget($fields); ?></div>
        <div class="people_area"><?php echo p4p_get_people_count_widget($fields); ?></div>
        <div class="progress_area"><?php echo p4p_get_progress_widget($fields, 5); ?></div>
    </div>
<?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function p4p_get_navigation_bar($postid, $postparent)
{
    if (empty($postid) || empty($postparent)) return;

    $siblings = p4p_get_navigation_pages($postid, $postparent);
    $parent = get_post($postparent);
    $parent_link = get_the_permalink($postparent);
    if (!empty($siblings["previous"])) {
        $previous_link = get_the_permalink($siblings["previous"]->ID);
    }
    if (!empty($siblings["next"])) {
        $next_link = get_the_permalink($siblings["next"]->ID);
    }

    ob_start();
?>
    <div class="php4_navigation_toolbar">
        <div class="panel">
            <?php if (!empty($previous_link)) { ?>
                <a class="nav_button previous" href="<?php echo $previous_link ?>" title="<?php echo $siblings["previous"]->post_title ?>">&laquo; <span>Plan</span> anterior</a>
            <?php } ?>
        </div>
        <div class="panel">
            <?php if (!empty($parent_link)) { ?>
                <a class="nav_button parent" href="<?php echo $parent_link ?>" title="<?php echo $parent->post_title ?>"> Ir a menú <span>de planes</span></a>
            <?php } ?>
        </div>
        <div class="panel">
            <?php if (!empty($next_link)) { ?>
                <a class="nav_button next" href="<?php echo $next_link ?>" title="<?php echo $siblings["next"]->post_title ?>">Siguiente <span>plan</span> &raquo;</a>
            <?php } ?>
        </div>
    </div>
<?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
