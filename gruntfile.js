module.exports = function (grunt) {
  require("jit-grunt")(grunt);
  const banner =
    "/*! ----------------------------\n" +
    " <%= pkg.description %>\n" +
    " Version: <%= pkg.version %>\n" +
    " Author: <%= pkg.author %>" +
    "\n---------------------------- */ \n\n";

  const styles_files = {
    "assets/p4p.min.css": ["assets/p4p.less"],
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    less: {
      production: {
        options: {
          banner: banner,
          compress: true,
        },
        files: styles_files,
      },
    },
    watch: {
      styles: {
        files: ["assets/**/*.less", "package.json", "gruntfile.js"],
        tasks: ["less"],
      },
    },
  });

  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.registerTask("production", ["less"]);
  grunt.registerTask("default", ["production", "watch"]);
};
